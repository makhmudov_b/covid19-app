import React, { useState } from 'react';
import { View, StyleSheet, ScrollView, Image, Text, ActivityIndicator } from 'react-native';
import global from '../../resources/global';
import HeaderBack from '../components/HeaderBack';

const UsefulInfoScreen = ({ navigation, route }: any): any => {
  const { article } = route.params;
  return (
    <>
      <View style={styles.container}>
        <HeaderBack navigation={navigation} />
        <ScrollView style={{ paddingVertical: 40 }}>
          <View style={styles.infoBox}>
            {!article ? (
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size="large" color={global.colors.mainColor} />
              </View>
            ) : (
              <>
                <View style={{ marginBottom: 15 }}>
                  <Image
                    source={{ uri: article.image }}
                    style={{ height: 200, borderRadius: 20, width: '100%', resizeMode: 'cover' }}
                    height={200}
                  />
                </View>
                <View style={{ marginLeft: 10, flex: 1 }}>
                  <Text style={styles.topText}>{article.title}</Text>
                  <Text style={styles.aboutText}>{article.description}</Text>
                </View>
              </>
            )}
          </View>
          <View style={{ marginBottom: 60 }} />
        </ScrollView>
      </View>
    </>
  );
};

export default UsefulInfoScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: global.colors.secondaryColor,
    position: 'relative',
  },
  infoBox: {
    backgroundColor: 'white',
    width: `92%`,
    borderRadius: 20,
    marginBottom: 15,
    position: 'relative',
    padding: 20,
    overflow: 'hidden',
    alignSelf: 'center',
  },
  topText: {
    fontFamily: global.fonts.bold,
    fontSize: 24,
    textAlign: 'center',
  },
  aboutText: {
    fontFamily: global.fonts.regular,
    fontSize: 18,
    color: '#202020',
    marginTop: 10,
  },
  bottomText: {
    fontSize: 13,
    color: '#202020',
    textAlign: 'center',
  },
});
