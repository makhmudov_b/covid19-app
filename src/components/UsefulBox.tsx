import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import global from '../../resources/global';

interface Props {
  navigation: any;
  article: any;
}
const UsefulBox = ({ navigation, article }: Props) => {
  return (
    <TouchableOpacity
      style={styles.infoBox}
      onPress={() => navigation.navigate('UsefulInfo', { article })}>
      <View>
        <Image
          source={{ uri: article.image }}
          style={{ height: 100, width: 100, borderRadius: 10 }}
          width={100}
          height={100}
        />
      </View>
      <View style={{ marginLeft: 10, flex: 1 }}>
        <Text style={styles.topText}>{article.title}</Text>
        <Text style={styles.aboutText}>{article.description}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default UsefulBox;
const styles = StyleSheet.create({
  infoBox: {
    backgroundColor: 'white',
    width: `92%`,
    borderRadius: 20,
    marginBottom: 15,
    position: 'relative',
    padding: 20,
    overflow: 'hidden',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  topText: {
    fontFamily: global.fonts.bold,
    fontSize: 12,
  },
  aboutText: {
    fontFamily: global.fonts.regular,
    fontSize: 12,
    color: '#979797',
    marginTop: 10,
  },
  bottomText: {
    fontSize: 13,
    color: '#202020',
    textAlign: 'center',
  },
});
